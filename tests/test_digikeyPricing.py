import pytest
import json
import pandas as pd
import numpy
from unittest.mock import AsyncMock
from digikeyPricing import create_parser, set_logger, convert_price_breaks_to_dict, get_price_breaks, get_price, get_item_price, main
from digikey_api import DigikeyProductDetailsSession

@pytest.fixture
def api_handle():
    return DigikeyProductDetailsSession()

@pytest.fixture
def break_dict():
    return {
        "BreakQuantity": [1, 10, 100],
        "UnitPrice": [10.0, 9.0, 8.0]
    }

def test_create_parser():
    parser = create_parser()
    assert parser.prog == "digikeyPricing"


def test_convert_price_breaks_to_dict():
    response = {
        "StandardPricing": [
            {"BreakQuantity": 1, "UnitPrice": 10.0},
            {"BreakQuantity": 10, "UnitPrice": 9.0},
            {"BreakQuantity": 100, "UnitPrice": 8.0}
        ]
    }
    result = convert_price_breaks_to_dict(response)
    assert result == {
        "BreakQuantity": [1, 10, 100],
        "UnitPrice": [10.0, 9.0, 8.0]
    }

def test_get_price(break_dict):
    price = get_price(break_dict, 5)
    assert price == 40.0