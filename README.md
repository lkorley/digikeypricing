# DigikeyPricing

This repository contains a set of Python scripts and modules to interact with the Digikey API and calculate the pricing of electronic components. The main components of this repository are:

1. `digikey_api.py`: A module containing the `DigikeyProductDetailsSession` class for interacting with the Digikey API.
2. `oauth2.py`: A module containing the `TokenHandler` class for handling OAuth2 authentication.
3. `digikeyPricing.py`: A script that reads a CSV file containing electronic components, queries the Digikey API for pricing information, and generates output files with the pricing details.

## Prerequisites

- Python 3.7+
- Required Python packages in `requirements.txt`

## Installation

1. Clone the repository:

```
git clone https://gitlab.com/lkorley/digikeypricing.git
cd digikeypricing
```

2. (Optional) Create and activate a virtual environment:

```
python -m venv venv
source venv/bin/activate
```

3. Install the required packages:

```
pip install -r requirements.txt
```

## Usage

### Digikey API

To interact with the Digikey API, create an instance of the `DigikeyProductDetailsSession` class and use its methods to fetch product details. You will need to provide your API credentials in the form of a JSON configuration file (e.g., `.conf.json`).

```
python
from digikey_api import DigikeyProductDetailsSession
import json

conf = json.loads(open(".conf.json").read())
api_session = DigikeyProductDetailsSession(**conf)

part_number = "12345"
response = api_session.ProductDetails(part_number)
```

### Digikey Pricing Tool

The digikeyPricing.py script accepts a CSV file containing a list of electronic components and queries the Digikey API for pricing information. The script generates two output files: `Matches.csv` containing valid entries and their prices, and `Unmatched.csv` containing invalid entries.

Here's how to use the script from the command line:

```
python digikeyPricing.py <filepath> [optional args]
```

Arguments:

filepath: Input CSV file with component details.

- -n, --rows: Number of CSV file rows to search for (default: all rows).
- -s, --sep: CSV file delimiter (default: ,).
- -c, --conf: API client configuration file (default: ./.conf.json).
- -l, --log: Logger level (D: Debug, I: Info, W: Warning, E: Error, default: W).
- -o, --outputdir: Path to save output files (default: ./).

Example usage:

```
python digikeyPricing.py components.csv -n 10 -s , -c .conf.json -l I -o output/
```

## Configuration

The Digikey API client configuration file (./.conf.json by default) should be a JSON file with the following keys:

- "id": Your Digikey API client ID.
- "secret": Your Digikey API client secret.
- "port": Local port to use for redirect, set when registering application
- "redirect_uri": callback uri for authorization code
- "auth_url": URL for user authorization
- "token_url": URL to exchange authorization code for access token
- "base_url": root URL for digike application
- "token_storage_path": Location to store token

These keys can be obtained by creating an account and registering for API access on the [Digikey website](https://developer.digikey.com/get_started).
