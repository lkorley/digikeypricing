import pandas
import os
import json
import digikey_api as digikey
import numpy
import logging
import math
import asyncio
from pathlib import Path

logger = logging.getLogger(__name__)

def create_parser():
    '''
    Parse cmd line arguments
    '''
    import argparse
    parser = argparse.ArgumentParser('digikeyPricing',usage='python digikeyPricing.py <filepath> [optional args]',description='Takes a csv of parts with "Quantity" and "Stock Code" columns. Input file is assumed to have header names in first row.\nQeuries digikey via api for items in csv and generates total price for found items printing them to screen.\nItems not found in the query are noted in a csv file [Unmatched.csv].')
    parser.add_argument('file',type=str,help="Input csv file")
    parser.add_argument('-n','--rows',type=int,default=None,help="Number of CSV file rows to search for")
    parser.add_argument('-s','--sep',type=str,default=',',help="CSV file delimator")
    parser.add_argument('-c','--conf',type=str,default='./.conf.json',help='API client configuration file')
    parser.add_argument('-l','--log',type=str,default='W',help="Logger level (D:Debug,I:Info,W:Warning,E:Error)")
    parser.add_argument('-o','--outputdir',type=str,default='./',help="Path to save output file")
    
    return parser

headers = {'X_DIGIKEY_Locale_Site':'UK',
        'X_DIGIKEY_Locale_Language':'en',
        'X_DIGIKEY_Locale_Currency':'GBP'}

def set_logger(level:str):
    if level == 'D':
        logging.basicConfig(level=logging.DEBUG)
    elif level == 'I':
        logging.basicConfig(level=logging.INFO)
    elif level == 'W':
        logging.basicConfig(level=logging.WARNING)
    elif level == 'E':
        logging.basicConfig(level=logging.ERROR)


def convert_price_breaks_to_dict(response)->dict:
    '''
    Takes in api response returns dictionary of price breaks
    Returns:
        dict: {'break_quantity':[],'unit_price':[],''}
    '''
    list_of_breaks = response['StandardPricing']
    return {k: [d[k] for d in list_of_breaks] for k in list_of_breaks[0]}

async def get_price_breaks(stock_code:str,api_handle:digikey.DigikeyProductDetailsSession)->dict:
    '''
    Call API and return dictionary of price breaks and unit costs
    Returns:
        dict: {'break_quantity':[],'unit_price':[],''}
    '''
    response = await api_handle.async_ProductDetails(stock_code,**headers)
    if not response:
        return None
    return convert_price_breaks_to_dict(response)

def get_price(break_dict,quantity)->float:
    '''
    Get unit price for appropriate break quantity
    '''
    #get maximum break quantity less than required quantity
    try:
        largest_below_idx = numpy.argmax(numpy.ma.MaskedArray(break_dict['BreakQuantity'],numpy.array(break_dict['BreakQuantity'],dtype=numpy.int32) <= quantity))
    except(TypeError,ValueError):
        logging.error(f"There was an issue with the Quantity value: {quantity}")
        return None
    return break_dict['UnitPrice'][largest_below_idx]*quantity

async def get_item_price(stock_code:str,quantity:int,api_handle:digikey.DigikeyProductDetailsSession)->float:
    '''
    Calculate price for line item
    Return total price for the requested quantity
    '''
    response_price_dict = await get_price_breaks(stock_code,api_handle)
    if not response_price_dict:
        return None
    
    #calculate price
    cost = get_price(response_price_dict,quantity)
    if not cost:
        logging.error(f"For Stock Code: {stock_code}")
        return None
    
    return cost



async def main(args=None):
    #parse args
    args = create_parser().parse_args(args)
    
    set_logger(args.log)
    
    print(f'Reading in csv file {args.file}')
    #grab csv input data as pandas dataframe
    bill_of_materials_df = pandas.read_csv(args.file,sep=args.sep,nrows=args.rows)

    #TODO check for duplicate stock codes
    
    #load config
    #authConf = json.loads(open(args.conf).read())
    #put config variables in environment
    #os.environ.update(authConf)
    apiSession = digikey.DigikeyProductDetailsSession(**json.loads(open(args.conf).read()))
    
    #loop through dataframe and store valid indexes
    print('Querying digikey API for provided Stock codes.\n-----------------\n------------------')
    
    bill_of_materials_df['Price'] = await asyncio.gather(*[get_item_price(item[1]['Stock Code'],item[1]['Quantity'],apiSession) for item in bill_of_materials_df.iterrows()])
    logging.info('\n----------------\n----------------\nQueries completed! Calculating Totals')
    
    #invalid entries
    invalid_df = bill_of_materials_df.query('Price != Price')
    save_path = Path(args.outputdir).joinpath('Unmatched.csv')
    invalid_df.to_csv(save_path,index=False)
    print(f'----------------------\nFound {len(invalid_df)} invalid Entries. \nSaving to "{save_path}"')
    #valid entries
    matches_df = bill_of_materials_df.dropna(subset=['Price'])
    num_valid = len(matches_df)
    total_price = matches_df['Price'].sum()
    matches_df = pandas.concat([matches_df,pandas.Series({'Price':total_price},index=matches_df.columns).fillna('')],ignore_index=True)
    save_path = Path(args.outputdir).joinpath('Matches.csv')
    matches_df.to_csv(save_path,index=False)
    print(f'-----------------\n{num_valid} Valid Entries Found.\nSaved to "{save_path}"\nTotal Price = {math.ceil(total_price*100)/100} GBP')
    
if __name__ == "__main__":
    asyncio.run(main())