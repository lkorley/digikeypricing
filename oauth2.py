import os 
import json 
import logging
import typing

from datetime import datetime, timezone
from pathlib import Path

import httpx

import ssl
from certauth.certauth import CertificateAuthority

from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlencode, urlparse, parse_qs

from webbrowser import open_new

logger = logging.getLogger(__name__)

class OauthException(Exception):
    pass

class OauthToken:
    def __init__(self,token) -> None:
        self._token = token
    
    @property
    def access_token(self):
        return self._token.get('access_token')
    
    #include refresh token access
    
    @property
    def expires(self):
        return datetime.fromtimestamp(self._token.get('expires'), timezone.utc)
    
    @property
    def type(self):
        return self._token.get('token_type')
    
    def expired(self) -> bool:
        return datetime.now(timezone.utc)>=self.expires
    
    def get_auth(self) -> str:
        return f'{self.type} {self.access_token}'
    
    def __repr__(self) -> str:
        return f'<|Token: Valid until {self.expires.astimezone().isoformat()}'
    
class HTTPHandler(BaseHTTPRequestHandler):
    """
    HTTP Server to handle OAuth redirect
    """
    def __init__(self, request, client_address, server,a_id,a_secret):
        self.app_id = a_id
        self.app_secret = a_secret
        self.auth_code = None
        super().__init__(request, client_address, server)
        
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        get_params = parse_qs(urlparse(self.path).query)
        if 'code' in get_params:
            self.auth_code = get_params['code'][0]
            self.wfile.write(bytes('<html>' +
                                   '<body>'
                                   '<h1>Feel free to Close this window.</h1>'+
                                   '<p>Auth code: '+ str(self.auth_code)+
                                   '</body></html>',
                                   'utf-8'
                                   ))
            self.server.auth_code = self.auth_code
            self.server.stop = 1
        else:
            raise OauthException(f'Failed to get authorization token in request: {self.path}')

class TokenHandler:
    """
    Handler and get OAuth token
    """
    def __init__(self,
                 id:str=None,
                 secret:str=None,
                 token_storage_path:str=None,
                 auth_url:str=None,
                 token_url:str=None,
                 port:int=None,
                 redirect_uri:str=None,*args,**kwargs) -> None:
        if not id or not secret:
            raise ValueError('CLIENT ID and SECRET must be set.\nCheck Your configuration file or pass them directly')
        
        if not token_storage_path or not Path(token_storage_path).exists():
            raise ValueError('STORAGE PATH must be set.\nCheck Your configuration file or pass them directly')
        
        if not port or not redirect_uri:
            raise ValueError('REDIRECT URI and PORT must be set.\nCheck Your configuration file or pass them directly')
        
        if not auth_url or not token_url:
            raise ValueError('AUTH URL and TOKEN URL must be set.\nCheck Your configuration file or pass them directly')
        
        self.auth_url = auth_url
        self.token_url = token_url
        self._id = id
        self._secret = secret
        self._storage_path = Path(token_storage_path)
        self._token_storage_path = self._storage_path.joinpath('token_stroage.json')
        self._ca_cert = self._storage_path.joinpath('digikey-api.pem')
        self._port = port
        self._redirect_uri = redirect_uri
        self._token = None
        
    def __generate_cert(self):
        """
        Create and return certificate for redirect
        Returns:
            (X509 | PKey | Any): filename for generated certificate
        """
        ca = CertificateAuthority('Python digikey-api CA', str(self._ca_cert), cert_cache=str(self._storage_path))
        return ca.cert_for_host('localhost')
    
    def __full_authorization_url(self) -> str:
        """
        Appends client id, response type and redirect uri to authorization url

        Returns:
            str: authorization url with client_id etc defined
        """
        params = {'client_id':self._id,
                  'response_type':'code',
                  'redirect_uri': self._redirect_uri}
        a_url = f'{self.auth_url}?{urlencode(params)}'
        return a_url
    
    def __code_for_token(self,code:str):
        """
        Exchange authentication code for oauth token
        Returns:
            json: api access token in json format
        """
        header = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:93.0) Gecko/20100101 Firefox/93.0',
                  'Content-type':'application/x-www-form-urlencoded'}
        post_data = {'grant_type':'authorization_code',
                     'code':code,
                     'client_id':self._id,
                     'client_secret':self._secret,
                     'redirect_uri':self._redirect_uri}
        
        try:
            req = httpx.post(self.token_url,headers=header,data=post_data)
            req.raise_for_status()
        except (httpx.RequestError,httpx.HTTPError):
            raise OauthException(f'TOKEN ERROR - Cannot request token with auth code {code}\nIt may have expired.')
        else:
            token_json = req.json()
        
        #timestamp for token lifetime
        token_json['expires'] = int(token_json['expires_in'])+datetime.now(timezone.utc).timestamp() -60

        return token_json
    
    #TODO handle refresh token
    
    def save(self,json_data:dict) -> None:
        """
        Save JSON output in storage location
        """
        with open(self._token_storage_path,'w') as f:
            json.dump(json_data,f)
            
    def check_access_token(self) -> typing.Union[OauthToken,None]:
        """
        Checks the storage location for an existing unexpired access token.
        Returns:
            OauthToken or None
        """
        try:
            if not self._token:
                with open(self._token_storage_path,'r') as f:
                    token_json = json.load(f)
                
                self._token = OauthToken(token_json)
            if self._token.expired():
                logger.debug('Oauth Token is stale')
                self._token = None
        except (EnvironmentError,json.decoder.JSONDecodeError):
            logger.warning("Oauth token storage doesn't exist or is mangled somehow.\nRequesting a new token.")
        
        return self._token
    
    def get_access_token(self) -> OauthToken:
        """

        Returns:
            OauthToken: 
        """
        # check for existing token
        self.check_access_token()
        if self._token is not None:
            return self._token.access_token
        
        # open authorization URL in browser
        authorization_url = self.__full_authorization_url()
        logger.info(f'Opening {authorization_url} in Browser')
        open_new(authorization_url)
        
        # make certificate
        cert_file = self.__generate_cert()
        
        # start httpserver ready to recieve auth_code at redirect_uri
        httpd = HTTPServer(('localhost', self._port), 
                           lambda request, address, server: HTTPHandler(request, address, server, self._id, self._secret))
        httpd.socket = ssl.wrap_socket(httpd.socket,certfile=str(Path(cert_file)),server_side=True)
        httpd.stop = 0
        
        # block till callback request
        while httpd.stop == 0:
            httpd.handle_request()
        httpd.server_close()
        
        # cleanup certificate
        try:
            os.remove(Path(cert_file))
            os.remove(self._ca_cert)
        except OSError as e:
            logger.error(f'Unable to remove certificates: {e}')
            
        # swap auth code for access token
        token_json = self.__code_for_token(httpd.auth_code)
        
        #save and return
        self.save(token_json)
        self._token = OauthToken(token_json)
        print(self._token.access_token)
        return self._token.access_token