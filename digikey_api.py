import os
import logging
from distutils.util import strtobool
import oauth2
from typing import *

import httpx

#TODO search manufacturerproduct details

logger = logging.getLogger(__name__)

class HTTPException(Exception):
    def __init__(self, status_code: int, message: str):
        self.status_code = status_code
        self.message = message
        super().__init__(f"{status_code} {message}")

    def __str__(self):
        return f"{self.status_code} {self.message}"

class DigikeyProductDetailsSession(oauth2.TokenHandler):
    
    def __init__(self, id: str = None, secret: str = None, token_storage_path: str = None, auth_url: str = None, token_url: str = None, port: int = None, redirect_uri: str = None,base_url:str=None, *args, **kwargs) -> None:
        self.base_url = base_url
        super().__init__(id, secret, token_storage_path, auth_url, token_url, port, redirect_uri, *args, **kwargs)
        self.get_access_token()
    
    @property
    def token(self):
        return self.get_access_token()
    
    def ProductDetails(self,
        digiKeyPartNumber: str,
        includes: Optional[str] = None,
        X_DIGIKEY_Locale_Site: Optional[str] = None,
        X_DIGIKEY_Locale_Language: Optional[str] = None,
        X_DIGIKEY_Locale_Currency: Optional[str] = None,
        X_DIGIKEY_Customer_Id: Optional[str] = None

    ):
        base_path = self.base_url
        path = f"/Search/v3/Products/{digiKeyPartNumber}"
        headers = {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": f"{self._token.get_auth()}",
            #"Authorization": self.token,
            "X-DIGIKEY-Client-Id": self._id,
            "X-DIGIKEY-Locale-Site": X_DIGIKEY_Locale_Site,
            "X-DIGIKEY-Locale-Language": X_DIGIKEY_Locale_Language,
            "X-DIGIKEY-Locale-Currency": X_DIGIKEY_Locale_Currency,
            "X_DIGIKEY_Customer_Id": X_DIGIKEY_Customer_Id

        }
        query_params: Dict[str, Any] = {"includes": includes}

        query_params = {key: value for (key, value) in query_params.items() if value is not None}
        headers = {key:value for (key,value) in headers.items() if value is not None}

        with httpx.Client(base_url=base_path, verify=True) as client:
            response = client.request(
                "get",
                httpx.URL(path),
                headers=headers,
                params=query_params,
            )

        if response.status_code != 200:
            logging.error(f" {digiKeyPartNumber} Query failed with status code: {response.status_code}")
            return None

        return response.json()

    async def async_ProductDetails(self,
        digiKeyPartNumber: str,
        includes: Optional[str] = None,
        X_DIGIKEY_Locale_Site: Optional[str] = None,
        X_DIGIKEY_Locale_Language: Optional[str] = None,
        X_DIGIKEY_Locale_Currency: Optional[str] = None,
        X_DIGIKEY_Customer_Id: Optional[str] = None
    ):
        base_path = self.base_url
        path = f"/Search/v3/Products/{digiKeyPartNumber}"
        headers = {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": f"{self._token.get_auth()}",
            "X-DIGIKEY-Client-Id": self._id,
            "X-DIGIKEY-Locale-Site": X_DIGIKEY_Locale_Site,
            "X-DIGIKEY-Locale-Language": X_DIGIKEY_Locale_Language,
            "X-DIGIKEY-Locale-Currency": X_DIGIKEY_Locale_Currency,
            "X_DIGIKEY_Customer_Id": X_DIGIKEY_Customer_Id
        }
        query_params: Dict[str, Any] = {"includes": includes}

        query_params = {key: value for (key, value) in query_params.items() if value is not None}
        
        headers = {key:value for (key,value) in headers.items() if value is not None}

        async with httpx.AsyncClient(base_url=base_path) as client:
            response = await client.request(
                "get",
                httpx.URL(path),
                headers=headers,
                params=query_params
            )

        if response.status_code != 200:
            logging.error(f" {digiKeyPartNumber} Query failed with status code: {response.status_code}")
            return None
            #raise HTTPException(response.status_code, f" failed with status code: {response.status_code}")

        return response.json()